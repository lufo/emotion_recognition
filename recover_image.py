# -*- coding: utf-8 -*-
__author__ = 'liuxuebo'

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

csv_file = '/Users/lufo/Dropbox/homework/CV/emotion/fer2013/fer2013.csv'
with open(csv_file) as fr:
    for i, row in enumerate(fr.readlines()[1:]):
        temp = row.split(',')
        emotion = temp[0]
        pixel = temp[1].split()
        pixel = list(map(int, pixel))
        image = np.array(pixel)
        image = image.reshape((48, 48))
        plt.imsave('/Users/lufo/Dropbox/homework/CV/emotion/fer2013/fer2013/{0}/{1}.jpg'.format(emotion, str(i)), image,
                   cmap=cm.gray)
