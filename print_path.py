# -*- coding: utf-8 -*-
__author__ = 'liuxuebo'
import os


def get_path(root_path):
    """
    输出root_path下所有文件的绝对路径
    :param root_path: 根路径
    :return:
    """
    path_list = []
    for i, dir_info in enumerate(os.walk(root_path)):
        for filename in dir_info[2]:
            if '.jpg' in filename:
                path_list.append(dir_info[0] + '/' + filename)
    return path_list


def main():
    root_path = '/Users/lufo/Dropbox/homework/CV/emotion/fer2013'
    path_list = get_path(root_path)
    with open('./fer_list.txt', 'w') as fw:
        for path in path_list:
            if '.jpg' in path:
                fw.write(path + '\n')


if __name__ == '__main__':
    main()
